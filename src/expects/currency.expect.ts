import { ConverterResponse } from '../data/currencyconverter';
export const noApiKeyResponseExpect: ConverterResponse = {
    status: 401,
    message: 'Invalid API key. Go to https://docs.rapidapi.com/docs/keys for more info.'
}

export const wrongApiKeyResponseExpect: ConverterResponse = {
    status: 403,
    message: 'You are not subscribed to this API.'
}

export const apiKey = 'd2e182eec3mshd8154c6d7b20407p14d21ejsncdad73d4030d';
export const path = 'availablecurrencies';

export const badRequestResponseExpect: ConverterResponse = {
    status: 400,
    message: 'Not sure'
}

export const notFoundResponseExpect: ConverterResponse = {
    status: 404,
    message: 'Not sure'
}
