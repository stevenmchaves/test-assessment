export interface CurrencyConverterRequestInterface {
  from_amount?: number;
  from?: string;
  to?: string;
}

export class CurrencyConverterRequest implements CurrencyConverterRequestInterface {
  from_amount: number | undefined;
  from: string | undefined;
  to: string | undefined;
  constructor(args?: CurrencyConverterRequestInterface) {
    if (args) {
      if (args.from_amount) { this.from_amount = args.from_amount; }
      if (args.from) { this.from = args.from; }
      if (args.to) { this.to = args.to; }
    }
  }
}

export interface CurrencyConverterResponseInterface extends CurrencyConverterRequestInterface {
  to_amount?: number;
}

export class CurrencyConverterResponse extends CurrencyConverterRequest implements CurrencyConverterResponseInterface {
  to_amount: number | undefined;
  constructor(args?: CurrencyConverterResponseInterface) {
    super(args)
    if (args) {
      if (args.to_amount) { this.to_amount = args.to_amount; }
    }
  }
}



export interface CurrencyInterface {
  id?: string;
  description?: string;
}

export class Currency implements CurrencyInterface {
  id: string | undefined;
  description: any;
  constructor(args?: CurrencyInterface) {
    if (args) {
      if (args.id) { this.id = args.id; }
      if (args.description) { this.description = args.description; }
    }
  }
}

export interface CurrencyResponseInterface {
  items?: CurrencyInterface[];
}

export interface HeaderInfoInterface {
  'x-rapidapi-key'?: string
  'X-RapidAPI-Host'?: string
}

export interface ConverterResponse {
  status: number;
  body?: string | CurrencyResponseInterface | CurrencyInterface,
  message?: string;
}