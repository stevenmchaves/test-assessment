
import { XMLHttpRequest } from 'xmlhttprequest-ts';
import { ConverterResponse } from '../data/currencyconverter';

export class CurrencyHttpsRequest {
  private options: any;
  
  public getOptions(): any {
    return this.options;
  }
  
  public setOptions(value: any) {
    this.options = value;
  }
  
  public updateHeaders(headers: any) {
    this.options.headers = headers;
  }
  /*
   * Constructor used to instantiate the class.
   */
  constructor(hostname: string, apiKey: any, path: string = '') {

    this.options = {
      method: 'GET',
      host: `${hostname}`,
      path: `${path}`,
      useQueryString: true,
      headers: {
        'x-rapidapi-host': hostname,
        Accept: 'application/json, text/javascript, text/plain',
      }
    };
  
    if(apiKey) {
      this.options.headers['x-rapidapi-key'] = apiKey;
    }
  }

  /*
   * String representation of the object
   */
  toString(): string {
    return 'CurrencyHttpsRequest: { options: ' + JSON.stringify(this.options) + ' }';
  }

  async getRequest(query: string | undefined = undefined): Promise<ConverterResponse> {
    this.options.method = 'GET';
    if (query && query.length > 0) {
      this.options.url += ('?' + query);
    }
    return await this.callApi(this.options, undefined);
  }

  async postRequest(body: any): Promise<ConverterResponse> {
    this.options.method = 'POST';
    return await this.callApi(this.options, JSON.stringify(body));
  }

  async putRequest(entry: string): Promise<ConverterResponse> {
    this.options.method = 'PUT';
    return await this.callApi(this.options, entry);
  }
  async deleteRequest(entry: string): Promise<ConverterResponse> {
    this.options.method = 'DELETE';
    if (entry && entry.length > 0) {
      this.options.path += '/' + entry;
    }
    return await this.callApi(this.options, '');
  }

  async callApi(options: any, body: string|undefined, timeout = 20000): Promise<ConverterResponse> {
    return new Promise<ConverterResponse>((resolve) => {
      const xhr = new XMLHttpRequest();
      xhr.timeout = timeout;
      xhr.withCredentials = true;
      let url = 'https://' + options.host;
      if (options.port) {
        url += ':' + options.port;
      }
      if (options.path) {
        url += options.path;
      }
      xhr.open(options.method, url);
      if (options.headers) {
        Object.keys(options.headers).forEach(key => {
          xhr.setRequestHeader(key, options.headers[key])
        });
      }
      xhr.onload = () => {
        resolve(this.responseParser(xhr.status, xhr.responseText));
      };

      xhr.ontimeout = evt => {
        resolve(this.errorResponse(xhr, 'Request took longer than expected.'));
      };
    
      xhr.onerror = evt => {
        resolve(this.errorResponse(xhr, 'Failed to make request.'));
      };
      if (this.options.method === 'POST' && body) {
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.send(body);
      } else {
        xhr.send();
      }
    });
  }

  private responseParser(status: number, rawData: any): ConverterResponse {
    let retValue: ConverterResponse = {
      status: status
    }
    let jsonData = rawData;
    try {
      if (rawData && rawData.trim().length > 0) {
        jsonData = JSON.parse(rawData);
      }
    } catch(err) {
      console.log('Response was not JSON. - ' + err);
    }
    if (jsonData.message) {
      retValue.message = jsonData.message;
    } else {
      retValue.body = jsonData;
    }
    return retValue;
  }

  private errorResponse(xhr: XMLHttpRequest, message: string | null = null): ConverterResponse {
    return {
      status: xhr.status,
      message: xhr.statusText,
      body: message || xhr.statusText
    };
  }
}
