export const hostname = 'currencyconverter.p.rapidapi.com'
export const apiKey = 'd2e182eec3mshd8154c6d7b20407p14d21ejsncdad73d4030d';
export const availPath = '/availablecurrencies';

export const happyCases: string[] = [
    // O from one currency should result in 0 from the other currency
    '?from=USD&from_amount=0&to=SEK',
    // basic USD 1 currency conversion to SEK
    '?from=USD&from_amount=1&to=SEK',
    // result should be 10 times as the return value from the case above
    '?from=USD&from_amount=10&to=SEK',
    // convert to the same currency should result in the same amount
    '?from=USD&from_amount=1&to=USD',
    // basic SEK 1 currency conversion to USD
    '?from=SEK&from_amount=1&to=USD',
];

export const badQueries: string[] = [
    // missing &
    '?from=USDfrom_amount=1&to=SEK',
    // negative from_amount
    '?from=USD&from_amount=-11&to=SEK',
    // missing & and =
    'from: USDL&, from_amount: 1, to: SEK',
     // missing ?
    'from=USD&from_amount=0&to:SEKL',
    // alphas in the from_amount
    '?from=EUR&from_amount=AAAA&to=SEK',
    // only from
    '?from=EUR',
    // only from_amount
    '?from_amount=0',
    // only from_amount and from
    '?from_amount=0&&from=EUR',
    // only to
    '?to=SEK',
    // only from_amount and to
    '?from_amount=0&to=SEK',
    // only from and to
    '?from=SED&to=USD',
    // SQL injection case - security test
     
];

export const largeQueries: string[] = [
    '?from=USD&from_amount=111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111&to=SEK',
    'from=EUR&from_amount=1&to=000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000SEK',
];
