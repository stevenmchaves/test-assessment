import { CurrencyHttpsRequest } from '../request/currency.requests';
import { availPath, hostname, apiKey, badQueries, largeQueries, happyCases } from '../inputs/currency.input';
import { badRequestResponseExpect, wrongApiKeyResponseExpect, noApiKeyResponseExpect } from '../expects/currency.expect';
import { delay } from '../helpers/helpers';

describe('Currency Converter API Testing', () => {
  beforeEach(async (done) => {
    await delay(5000);
    done();
  });

  describe('Header Cases', () => {
    describe('Authorization - ERROR Code 401', () => {
      test('sending undefined apikey for default GET converter', async (done) => {
        const request = new CurrencyHttpsRequest(hostname, undefined);
        const response = await request.getRequest();
        expect(response).toEqual(noApiKeyResponseExpect);
        done();
      });

      test('sending no apikey for default GET available currencies', async (done) => {
        const request = new CurrencyHttpsRequest(hostname, '', availPath);
        const response = await request.getRequest();
        expect(response).toEqual(noApiKeyResponseExpect);
        done();
      });
    });

    describe('Forbidden - ERROR Code 403', () => {

      test('sending wrong apikey for default GET converter', async (done) => {
        const request = new CurrencyHttpsRequest(hostname, 'retro');
        const response = await request.getRequest();
        expect(response).toEqual(wrongApiKeyResponseExpect);
        done();
      });

      test('sending wrong apikey for default GET available currencies', async (done) => {
        const request = new CurrencyHttpsRequest(hostname, 'any', availPath);
        const response = await request.getRequest();
        expect(response).toEqual(wrongApiKeyResponseExpect);
        done();
      });
    });

    describe('Other Bad Headers', () => {
      test('sending no xpath host for default GET converter', async () => {
        const request = new CurrencyHttpsRequest(hostname, apiKey);
        const optionsHeaders = request.getOptions().headers;
        delete optionsHeaders['x-rapidapi-host'];
        request.updateHeaders(optionsHeaders);
        const response = await request.getRequest();
        expect(response).toEqual(noApiKeyResponseExpect);
      });

      test('sending no host for default GET available currencies', async () => {
        const request = new CurrencyHttpsRequest(hostname, apiKey, availPath);
        const optionsHeaders = request.getOptions().headers;
        delete optionsHeaders['x-rapidapi-host'];
        request.updateHeaders(optionsHeaders);
        const response = await request.getRequest();
        expect(response).toEqual(noApiKeyResponseExpect);
      });
    });
  });

  describe('Happy Paths', () => {
    test('sending default GET converter with no query', async () => {
      const request = new CurrencyHttpsRequest(hostname, apiKey);
      const response = await request.getRequest();
      console.log(response);
      expect(response).toEqual('hello');
    });

    happyCases.forEach(happyCase => {
      test(`sending default GET converter with ${happyCase} query`, async () => {
        const request = new CurrencyHttpsRequest(hostname, apiKey);
        const response = await request.getRequest(happyCase);
        console.log(response);
        expect(response).toEqual('hello');
      });
    });

    test('sending default GET available currencies', async () => {
      const request = new CurrencyHttpsRequest(hostname, apiKey, availPath);
      const response = await request.getRequest();
      console.log(response);
      expect(response).toEqual(noApiKeyResponseExpect);
    });
  });

  describe('Negative Cases', () => {
    const request = new CurrencyHttpsRequest(hostname, apiKey);
    badQueries.forEach(badQuery => {
      test(`sending ${badQuery} to GET currency converter as the query`, async () => {
        const response = await request.getRequest(badQuery);
        console.log(response);
        expect(response).toEqual(badRequestResponseExpect);
      });
    });

    largeQueries.forEach(largeQuery => {
      test(`sending ${largeQuery} to GET currency converter as the query`, async () => {
        const response = await request.getRequest(largeQuery);
        console.log(response);
        expect(response).toEqual(badRequestResponseExpect);
      });
    });

    test('sending default GET available currencies try with a query', async () => {
      const availRequest = new CurrencyHttpsRequest(hostname, apiKey, availPath);
      const response = await availRequest.getRequest('?id=SEK');
      console.log(response);
      expect(response).toEqual(badRequestResponseExpect);
    });
  
  });
});