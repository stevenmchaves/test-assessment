module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testTimeout: 30000,
  verbose: true,
  reporters: [
    'default',
    ['jest-html-reporters', {
      'publicPath': './html-report',
      'filename': 'report.html',
      'expand': true
    }]
  ]
};