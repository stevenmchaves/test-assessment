# test-assessment

The API being tested basically doesn't work.

## Following issues are seen throughout the evaluation of the API
- Major latency
- Timeouts 504 Server side
- Cannot handle multiple calls in rapid succession of even within a few hundred milliseconds from another one. Causing 429
- Concurrency issues


## Execute API tests
You need to have nodejs installed.
```bash
npm run test
```
This command will install all the rest of the necessary packages.
This uses jest.
A html report with bllprinted the html-report/report.html
Execution takes over 5 minutes as I set each timeout for a test after 30 seconds and each request timeout after 20 seconds.
Hopefully the API is corrected and the timeouts won't happen.

I was not able to get any accurate expected outputs. I tried to determine what they might be.
## Test cases
See the description of each describe and test for the test cases within the currencyapi.test.ts along with the currency.input.ts

